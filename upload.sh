#!/bin/bash

JAR_FILE="build/libs/SimpleRequestsBot.jar"
PACKAGE_NAME="SimpleRequestsBot"
PACKAGE_VERSION="1.0"

curl --header "PRIVATE-TOKEN: $CI_JOB_TOKEN" \
    --upload-file $JAR_FILE \
    "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/packages/generic/$PACKAGE_NAME/$PACKAGE_VERSION/${JAR_FILE##*/}"
